# SOR
Se implemento una topologia con 6 nodos

n0--+        +--n6
    |        |
n1---n4 ---n5---n7
    |        |
n2--+        +--n8

TCP:
    n1 --- n6
    n2 --- n7

UDP:
    n2 --- n8